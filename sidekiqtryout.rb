require 'sinatra'
require './sidekiqtryout/worker'
require './sidekiqtryout/db'

require "sinatra/reloader" if development?

set :bind, '0.0.0.0'

get '/' do
  'Hello world'
end

post '/work' do
  pid = Random.rand(9999)
  Sidekiqtryout::Worker.perform_async(pid)
  { pid: pid, status: 'pending', data: nil }.to_json
end

get '/work/:id' do
  id = params[:id]
  result = Sidekiqtryout::Db.get(id)
  if result.nil?
    status 404
    error = { code: 404, message: 'No job found with that pid' }.to_json
    body error
  else
    JSON.parse(result).merge(pid: id).to_json
  end
end