require 'sidekiq'
require './sidekiqtryout/db'

module Sidekiqtryout
  class Worker
    include ::Sidekiq::Worker

    def perform(pid)
      Db.set(pid, { status: 'pending', data: nil }.to_json)
      sleep(Random.rand(20) + 2)

      data = { pid: pid, number: Random.rand(99999) }
      Db.set(data[:pid], { status: 'done', data: data[:number] }.to_json)

      Sidekiq::Logging.logger.info data.to_json
    end
  end
end
