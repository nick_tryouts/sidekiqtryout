require 'redis'

module Sidekiqtryout
  module Db
    @redis = Redis.new

    def self.set(k, v)
      @redis.set(k, v)
    end

    def self.get(k)
      @redis.get(k)
    end
  end
end

