FROM ruby:2.3.3

WORKDIR /app

RUN apt-get update && \
    # apt-get install -y redis-server \
    apt-get install -y ruby-sidekiq

ADD ./run.sh /root/bootstrap.sh

EXPOSE 4567

ENTRYPOINT /root/bootstrap.sh